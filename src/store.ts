import Vue from 'vue'
import Vuex, { ActionTree, ActionContext, MutationTree } from 'vuex'

Vue.use(Vuex)

interface State {
  hex: string,
  question: {
    question: string,
    answer: string | undefined,
    answerIndex: Array<number>
  },
  questionBank: Array<{ question: string, answer: string | undefined, answerIndex: Array<number> }>,
  correct: boolean,
  cheated: boolean
}

const state: State = {
  hex: '',
  question: { question: 'What is the answer to life, the universe, and everything?', answer: '42', answerIndex: [0, 0] },
  questionBank: [],
  correct: false,
  cheated: false
}

const mutations: MutationTree<State> = {
  setNewQuestion: (state) => {
    state.question = state.questionBank[Math.floor(Math.random() * state.questionBank.length)]
  },
  questionAnswered: (state) => {
    state.questionBank = state.questionBank.filter((q) => {
      return q !== state.question
    })
  }
}

const actions: ActionTree<State, any> = {
  updateHex: (context: ActionContext<State, any>, payload: string) => {
    context.state.hex = payload
  },
  updateCorrect: (context: ActionContext<State, any>, payload: boolean) => {
    context.state.correct = payload
  },
  updateQuestionBank: (context: ActionContext<State, any>, payload: Array<{ question: string, answer: string | undefined, answerIndex: Array<number> }>) => {
    context.state.questionBank = payload
  },
  updateCheated: (context: ActionContext<State, any>, payload: boolean) => {
    context.state.cheated = payload
  }
}

export default new Vuex.Store<State>({ state, actions, mutations })

/* Good store examples:
https://github.com/fullstackio/awesome-fullstack-tutorials/blob/master/vue/managing_state_01/vuex-store/src/store.js
https://github.com/hmexx/vue_typescript_starter_kit/blob/master/src/store/store.ts
*/
