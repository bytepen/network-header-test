module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/network-header-test/'
    : '/'
}
