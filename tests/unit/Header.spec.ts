import { shallowMount } from '@vue/test-utils'
import Header from '@/components/Header.vue'

describe('HelloWorld.vue', () => {
  it('renders div with offset markers', () => {
    const l2header = '000c29a35c92000c291aad3d0806'
    const wrapper = shallowMount(Header, {
      propsData: { l2header }
    })
    expect(wrapper.text()).toContain('OFFSET')
    expect(wrapper.text()).toContain('00')
    expect(wrapper.text()).toContain('15')
    expect(wrapper.text()).toContain('0000')
  }
  )
})
